class Employee {
	constructor(name, age, salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	get name() {
		return this.name;
	}
	set name(value) {
		console.log('name: ' + value);
	}

	get age() {
		return this.age;
	}
	set age(value) {
		console.log('age: ' + value);
	}
	
	get salary() {
		return this.salary;
	}
	set salary(value) {
		console.log('salary: ' + value);
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}
	get lang() {
		return this.lang;
	}
	set lang(value) {
		console.log('lang: ' + value);
	}
	get salary() {
		return this.salary;
	}
	set salary(value) {
		console.log('salary: ' + value * 3);
	}
	
}
let programmer = new Programmer('ivan', 86, 3500, ['es', 'uk']);
let programmer2 = new Programmer('toha', 36, 2500, ['ua', 'pol']);
let programmer3 = new Programmer('max', 25, 1500, ['pol', 'es']);


console.log(programmer);
console.log(programmer2);
console.log(programmer3);

